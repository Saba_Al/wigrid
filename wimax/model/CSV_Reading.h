/*
 * CVS_Reading.cc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Jahanzeb Farooq <jahanzeb.farooq@sophia.inria.fr>
 *          Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                               <amine.ismail@UDcast.com>
 * Modified by: Fariba Aalamifar <faribaa@ece.ubc.ca>
 * MITACS Internship project @ Powertech Labs Inc., Surrey, BC, CA. 2014-2015
 * Description: Read CSV CPE information file.
 * Date: Apr. 10, 2014
 */

#ifndef CSV_F
#define CSV_F

#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace ns3;

std::map <const char*,ServiceFlow::SchedulingType> CSV_Reading(const char* , vector<Node_Data>,
		std::map <const char*,double>&);

void Read_New_Sites(const char*, vector<Node_Data> &);

#endif
