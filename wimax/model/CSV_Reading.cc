/*
 * CVS_Reading.cc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Jahanzeb Farooq <jahanzeb.farooq@sophia.inria.fr>
 *          Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                               <amine.ismail@UDcast.com>
 * Modified by: Fariba Aalamifar <faribaa@ece.ubc.ca>
 * MITACS Internship project @ Powertech Labs Inc., Surrey, BC, CA. 2014-2015
 * Description: Read CSV CPE information file.
 * Date: Apr. 10, 2014
 */
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <map>
#include <vector>

using namespace std;
using namespace ns3;

std::map <const char*,ServiceFlow::SchedulingType> CSV_Reading(const char* File_Name,
		vector<Node_Data> CPEs_Info, std::map <const char*,double> &numRTUs)
{
	NS_LOG_UNCOND("opening file");
	std::ifstream file ( File_Name ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
	NS_LOG_UNCOND("opened file");
	std::string value;
	//int lineNumber=0;
	int flag = 0;
	int cnt = 0;
	//int num = 0;
	std::map<const char*,ServiceFlow::SchedulingType> serviceProfile;
	while ( file.good() )
	{
		//NS_LOG_UNCOND("file is good! Yes" );
		//lineNumber++;
		flag = 0;
		for (int i=0;i<4;i++)
		{
			getline ( file, value, ':' ); // read a string until next ":", see http://www.cplusplus.com/reference/string/getline/
			//			if (i==4)
//				Height = std::atof(value.c_str());
		//	cout << value.c_str() << ":::";
			if (i==1)
			{
				for (uint j = 0; j < CPEs_Info.size(); j++)
				{
				//	cout << "value:" << value.c_str() << "," << "j:" << j << "," << CPEs_Info.at(j).location.c_str() << endl;
					if(std::strcmp((const char*)value.c_str(),(const char*)CPEs_Info.at(j).location.c_str())==0)
					{
						flag = 1;
						cnt = j;
					}
					//serviceProfile[cnt][cnt] = (const char*)CPEs_Info.at(j).location.c_str();
				}
				//cout << "flag: " << flag << ",";//CPEs_Info.at(cnt).location.c_str() <<
			}
			else if (i==2 && flag ==1)
			{
				if (strcmp(value.c_str(),"RT")==0)
					serviceProfile[CPEs_Info.at(cnt).location.c_str()] = ServiceFlow::SF_TYPE_RTPS;
				else if (strcmp(value.c_str(),"UGS")==0)
					serviceProfile[CPEs_Info.at(cnt).location.c_str()] = ServiceFlow::SF_TYPE_UGS;
						//file.close();
			}
			else if (i==3 && flag ==1)
			{
				numRTUs[CPEs_Info.at(cnt).location.c_str()] = std::atof(value.c_str());
				cout << "numRTUs[" << CPEs_Info.at(cnt).location.c_str() << "]="
						 <<  numRTUs[CPEs_Info.at(cnt).location.c_str()] << endl;
			}
//			if (i==2)
//				y=(std::atof(value.c_str())+79)*79.5;
//
//			if (i==5 && std::strcmp(value.c_str(),"YES")==1)
//				{
//					ssNum++;
//					ssPositionAlloc ->Add(Vector(x, y, z));
//					ssAntennaHeight ->push_back(Height);
//				}
//			if (i==6 && std::strcmp(value.c_str(),"YES")==1)
//				{
//					bsNum++;
//					bsPositionAlloc ->Add(Vector(x, y, z));
//					bsAntennaHeight ->push_back(Height);
//				}
//
//			NS_LOG_UNCOND(lineNumber << "," << i << ":" << value << std::endl);
		}//end for(j)
		//cout << endl;
		//num++;
	}//endwhile
//
//
//	    //cout << string( value, 1, value.length()-2 ); // display value removing the first and the last character from it
//	}
//

	/*for(int i = 0; i < nodes.GetN(); i++)
	{
		nodes.Get(i)->
	}*/
		//NS_ASSERT("location were not found in the csv file!");

		file.close();
		return(serviceProfile);
}

void Read_New_Sites(const char* File_Name, vector<Node_Data> &CPEs_Info)
{
	std::ifstream file ( File_Name ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
	//NS_LOG_UNCOND("opened file");
	std::string value;
	int lineno=0;
	while ( file.good() )
	{
		lineno++;
		Node_Data CPE;
		for (int i=0;i<7;i++)
		{
			getline ( file, value, ',' ); // read a string until next comma: http://www.cplusplus.com/reference/string/getline/
			//NS_LOG_UNCOND("line:"<<lineno<<",col:"<<i<<",val:"<<value);
			switch(i)
			{
			case 0:
				CPE.location = value.substr(2); //remove \r\n
				break;
			case 1:
				CPE.latitude = degree_to_Kmeter(atof(value.c_str()),true);
				break;
			case 2:
				CPE.longtitude = degree_to_Kmeter(atof(value.c_str()),false);
				break;
			case 3:
				CPE.TxFreq = atof(value.c_str());
				break;
			case 4:
				CPE.sectNum = atoi(value.c_str());
				break;
			case 5:
				CPE.elevation = atof(value.c_str());
				break;
			case 6:
				CPE.TxAntennaHeight = atof(value.c_str());
				break;
			default:
				break;
			}
		}
		if (file.eof())
			continue;
		CPEs_Info.push_back(CPE);
	}
}
