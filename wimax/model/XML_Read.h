#ifndef XML_H
#define XML_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "ns3/ptr.h"
#include "ns3/core-module.h"

using namespace std;

namespace ns3{
double degree_to_Kmeter(double ddmmss, bool latitude);
class Node_Data{  //Base class for BS and CPE Data.
public:
	Node_Data();
	~Node_Data();
	void print();
	string location;
	double longtitude;
	double latitude;
	double elevation;
	double RxFreq, TxFreq;
	double RxGain, TxGain;
	double TxPower;
	double TxAntennaHeight, RxAntennaHeight;
	bool bs;
	int sectNum;
};

//class BS_Data : public Node_Data {
//public:
//	BS_Data();
//	~BS_Data();
//	void print();
//};

//class CPE_Data : public Node_Data{
//public:
//	CPE_Data();
//	~CPE_Data();
//	string BS_Name;
//	//BS_Data *myBS;
//	int sectNum;
//	void print();
//};

void Xml_Read(const char*, vector<Node_Data> &, vector<Node_Data> &);
void Xml_Read_Freq_Compatible(const char*, vector<Node_Data> &, vector<Node_Data> &);
}
#endif
