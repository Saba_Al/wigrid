var searchData=
[
  ['qos_2dtest_2ecc',['qos-test.cc',['../qos-test_8cc.html',1,'']]],
  ['qos_5fparameter_5fset_5ftype',['QoS_Parameter_Set_Type',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a693fa7df3607cf9155c9abf5cc6a7b44',1,'ns3::SfVectorTlvValue']]],
  ['queueelement',['QueueElement',['../structns3_1_1WimaxMacQueue_1_1QueueElement.html#a3c3e5f03f369c2cd6077a2081c676fc7',1,'ns3::WimaxMacQueue::QueueElement::QueueElement(void)'],['../structns3_1_1WimaxMacQueue_1_1QueueElement.html#a0f5bcfc0d8b70394ff669ec974759837',1,'ns3::WimaxMacQueue::QueueElement::QueueElement(Ptr&lt; Packet &gt; packet, const MacHeaderType &amp;hdrType, const GenericMacHeader &amp;hdr, Time timeStamp)']]],
  ['queueelement',['QueueElement',['../structns3_1_1WimaxMacQueue_1_1QueueElement.html',1,'ns3::WimaxMacQueue']]]
];
