var searchData=
[
  ['sched_5ftype_5fmbqos',['SCHED_TYPE_MBQOS',['../classns3_1_1WimaxHelper.html#a27a40a8f601900126156781c2ca79406ade891904742c17bf21ef7f6e5d1c7ddc',1,'ns3::WimaxHelper']]],
  ['sched_5ftype_5frtps',['SCHED_TYPE_RTPS',['../classns3_1_1WimaxHelper.html#a27a40a8f601900126156781c2ca79406ac3c001b603746dc6b65b27b99419a315',1,'ns3::WimaxHelper']]],
  ['sched_5ftype_5fsimple',['SCHED_TYPE_SIMPLE',['../classns3_1_1WimaxHelper.html#a27a40a8f601900126156781c2ca79406afd3166736b35190178c45686575fa65b',1,'ns3::WimaxHelper']]],
  ['sdu_5fsize',['SDU_Size',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a3608be14b8ba4f5081671ce2f820afce',1,'ns3::SfVectorTlvValue']]],
  ['service_5fclass_5fname',['Service_Class_Name',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a378886d44f21f49919845d3204a9cca9',1,'ns3::SfVectorTlvValue']]],
  ['service_5fflow_5fscheduling_5ftype',['Service_Flow_Scheduling_Type',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7ad6aa9ed8976f134fcd3b2d1db6c735d3',1,'ns3::SfVectorTlvValue']]],
  ['sf_5fdirection_5fdown',['SF_DIRECTION_DOWN',['../classns3_1_1ServiceFlow.html#ae14b8dc8bb371bad10fe078110655d4fa14cabffe872245d876df31d3f63fc8b2',1,'ns3::ServiceFlow']]],
  ['sf_5fdirection_5fup',['SF_DIRECTION_UP',['../classns3_1_1ServiceFlow.html#ae14b8dc8bb371bad10fe078110655d4fadbdc2e453d8d7a126f89ecedca018714',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5factive',['SF_TYPE_ACTIVE',['../classns3_1_1ServiceFlow.html#a95a18bd8cae3a4eaa3568dad45ae941da68cc7128fe19a5d911aace393cc12bd5',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fadmitted',['SF_TYPE_ADMITTED',['../classns3_1_1ServiceFlow.html#a95a18bd8cae3a4eaa3568dad45ae941da45207ce14ef69b52dbf86163cde22079',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fall',['SF_TYPE_ALL',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235aaf7e58e43027cc9d351cd100a9d6dee3',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fbe',['SF_TYPE_BE',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235af93a8bd8fce654e688f957f6f362e5c7',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fnone',['SF_TYPE_NONE',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235a1c9639853c5529ae013feddcae94a4c6',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fnrtps',['SF_TYPE_NRTPS',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235a7f8577f851a9f01d159442a3a3fcdf48',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fprovisioned',['SF_TYPE_PROVISIONED',['../classns3_1_1ServiceFlow.html#a95a18bd8cae3a4eaa3568dad45ae941daf173379864cf3570db60adecc867fd66',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5frtps',['SF_TYPE_RTPS',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235a0e98ff713b932a029acad7e5b24bbf55',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fugs',['SF_TYPE_UGS',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235a969e0b62fa12fef1dbb23913744ed594',1,'ns3::ServiceFlow']]],
  ['sf_5ftype_5fundef',['SF_TYPE_UNDEF',['../classns3_1_1ServiceFlow.html#a7990ba10be1e098328fd1e6382a26235a106aef1addc1df41017d966574b32a4d',1,'ns3::ServiceFlow']]],
  ['sfid',['SFID',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a3a1c1c00258b4efc6901ef23b75eef56',1,'ns3::SfVectorTlvValue']]],
  ['short_5fpreamble',['SHORT_PREAMBLE',['../classns3_1_1BaseStationNetDevice.html#a1043912af2173c4026f8d3f5dc87df3ba27b7531027ce0fc60de828ea6bca0656',1,'ns3::BaseStationNetDevice']]],
  ['simple_5fphy_5ftype_5fofdm',['SIMPLE_PHY_TYPE_OFDM',['../classns3_1_1WimaxHelper.html#a42439a8e9f959fd3f5bdf698d248f71aa6884df5358e269f2339f63203851ccb7',1,'ns3::WimaxHelper']]],
  ['simpleofdmwimaxphy',['simpleOfdmWimaxPhy',['../classns3_1_1WimaxPhy.html#a30c76d3d1853c49c26edef12338cf6b4a4acb2a07b3261576bb02d62eb14fa5b0',1,'ns3::WimaxPhy']]],
  ['simplewimaxphy',['SimpleWimaxPhy',['../classns3_1_1WimaxPhy.html#a30c76d3d1853c49c26edef12338cf6b4a460db1d63213d1adb1d974a63ee6e018',1,'ns3::WimaxPhy']]],
  ['ss_5fstate_5facquiring_5fparameters',['SS_STATE_ACQUIRING_PARAMETERS',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4afbd36119d59678d1f5e448ff7b681cd8',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fadjusting_5fparameters',['SS_STATE_ADJUSTING_PARAMETERS',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4ab966447e17e355aff9bf29f0a92bd4a5',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fidle',['SS_STATE_IDLE',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a746a6c26cc18630753a923ba121b4824',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fregistered',['SS_STATE_REGISTERED',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a8e471619acd464651860d0d53cc6b56f',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fscanning',['SS_STATE_SCANNING',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a0eafcdd49c5f8b895597a76fa7d76aab',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fstopped',['SS_STATE_STOPPED',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a22e031574426ab38a8038231746ac57a',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fsynchronizing',['SS_STATE_SYNCHRONIZING',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4aeadd4611fbcf29b6cdb387e3dc77c32f',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5ftransmitting',['SS_STATE_TRANSMITTING',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a2dbc017634791633e312076d96dacb6b',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fwaiting_5finv_5frang_5fintrvl',['SS_STATE_WAITING_INV_RANG_INTRVL',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4aba899a8d6171ee1a9a924dde6c62f16f',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fwaiting_5freg_5frang_5fintrvl',['SS_STATE_WAITING_REG_RANG_INTRVL',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4a49bac6877dbda9589f7576e42cf5c15f',1,'ns3::SubscriberStationNetDevice']]],
  ['ss_5fstate_5fwaiting_5frng_5frsp',['SS_STATE_WAITING_RNG_RSP',['../classns3_1_1SubscriberStationNetDevice.html#af9f145bc05df1f18610a3d4b61ff9ee4acb68c803df0eb6a0f4cc41389f981db8',1,'ns3::SubscriberStationNetDevice']]]
];
