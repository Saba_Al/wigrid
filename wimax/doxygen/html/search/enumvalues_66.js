var searchData=
[
  ['fixed_5flength_5fversus_5fvariable_5flength_5fsdu_5findicator',['Fixed_length_versus_Variable_length_SDU_Indicator',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a067be329e4d6013e4c1d1e71758d687c',1,'ns3::SfVectorTlvValue']]],
  ['frame_5fduration_5f10_5fms',['FRAME_DURATION_10_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4ae4800fe9d5b82ffb7c3e4f645de541be',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f12_5fpoint_5f5_5fms',['FRAME_DURATION_12_POINT_5_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a7e664cff7e9fc46eab639ea078130904',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f20_5fms',['FRAME_DURATION_20_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a1ba3ca55fc897ace9c0ab62f63fd1550',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f2_5fpoint_5f5_5fms',['FRAME_DURATION_2_POINT_5_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a8088f603308b22c4b3267ba8a0d3cb23',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f4_5fms',['FRAME_DURATION_4_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a31a8abe3f7708b2686cc2646d5702342',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f5_5fms',['FRAME_DURATION_5_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a0f6e0119b9e8ee1cbe3e6770b8fc520a',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['frame_5fduration_5f8_5fms',['FRAME_DURATION_8_MS',['../classns3_1_1SimpleOfdmWimaxPhy.html#a81778c2233487351f2136fe80c63e1e4a114739c4a5e1bf73fab2a04b24809096',1,'ns3::SimpleOfdmWimaxPhy']]],
  ['friis_5fpropagation',['FRIIS_PROPAGATION',['../classns3_1_1SimpleOfdmWimaxChannel.html#ad8299e6adf4848b1cf213df963e94842a3a15cb0de31fe3061af5cb809f52356b',1,'ns3::SimpleOfdmWimaxChannel']]]
];
