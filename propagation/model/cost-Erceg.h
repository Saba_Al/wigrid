/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Name        : cost-Erceg.h
 * Version 	   : 1.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Fariba Aalamifar <faribaa@ece.ubc.ca>
 * MITACS Internship project @ Powertech Labs Inc., Surrey, BC, CA. 2014-2015
 *
 * Description:  ERCEG-B Pathloss model implementation.
 * Date: 10 Mar. 2015
 */

#ifndef Erceg_PROPAGATION_LOSS_MODEL_H
#define Erceg_PROPAGATION_LOSS_MODEL_H

#include "ns3/nstime.h"
#include "ns3/propagation-loss-model.h"

namespace ns3 {

/**
 *
 * This model is suitable for working frequencies near to 2 GHz.This propagation model
 * is categorized in three classes. category A is for hilly train with heavy tree densities.
 * Category B is for hilly train with light tree density or for flat train with medium to heavy tree density.
 * Category C is for flat train with light tree density.
 * The current values are for Type C but can easily be changed to other type by changin ae, be and c parameters in ErcegPropagationLossModel::GetLoss (Ptr<MobilityModel> a, Ptr<MobilityModel> b)
 * For more information, look at:
 * Raj Jain, "Channel Models: A Tutorial," WiMAX Forum AATG, February 2007, Page 12.
 *
 *
 */

class ErcegPropagationLossModel : public PropagationLossModel
{

public:
  static TypeId GetTypeId (void);
  ErcegPropagationLossModel ();
  enum Environment
  {
    A, B, C
  };

  /**
   * \param a the mobility model of the source
   * \param b the mobility model of the destination
   * \returns the propagation loss (in dBm)
   */
  double GetLoss (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const;
  void SetBSAntennaHeight (double height);
  void SetSSAntennaHeight (double height);
  void SetEnvironment (Environment env);
  void SetLambda (double lambda);
  void SetMinDistance (double minDistance);
  double GetBSAntennaHeight (void) const;
  double GetSSAntennaHeight (void) const;
  Environment GetEnvironment (void) const;
  double GetMinDistance (void) const;
  double GetLambda (void) const;
  void SetLambda (double frequency, double speed);
  double GetShadowing (void);
  void SetShadowing (double shadowing);
private:
  virtual double DoCalcRxPower (double txPowerDbm, Ptr<MobilityModel> a, Ptr<MobilityModel> b) const;
  virtual int64_t DoAssignStreams (int64_t stream);
  double m_BSAntennaHeight; // in meter
  double m_SSAntennaHeight; // in meter
  double C;
  double m_lambda;
  Environment m_environment;
  double m_minDistance; // in meter
  double m_frequency; // frequency in MHz
  double m_shadowing;

};

}

#endif /* ErcegPROPAGATIONMODEL_H */
