/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Name        : cost-Erceg.cc
 * Version 	   : 1.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Fariba Aalamifar <faribaa@ece.ubc.ca>
 * MITACS Internship project @ Powertech Labs Inc., Surrey, BC, CA. 2014-2015

 * Description:  ERCEG-B Pathloss model implementation.
 * Date: 10 Mar. 2015
 */

#include "ns3/propagation-loss-model.h"
#include "ns3/log.h"
#include "ns3/mobility-model.h"
#include "ns3/double.h"
#include "ns3/pointer.h"
#include <cmath>
#include <string>
#include "cost231-propagation-loss-model.h"
#include "cost-Erceg.h"


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ErcegPropagationLossModel");
NS_OBJECT_ENSURE_REGISTERED (ErcegPropagationLossModel);

TypeId
ErcegPropagationLossModel::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::ErcegPropagationLossModel")

    .SetParent<PropagationLossModel> ()

    .AddConstructor<ErcegPropagationLossModel> ()

    .AddAttribute ("Lambda",
                   "The wavelength  (default is 2.3 GHz at 300 000 km/s).",
                   DoubleValue (300000000.0 / 2.3e9),
                   MakeDoubleAccessor (&ErcegPropagationLossModel::m_lambda),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("Frequency",
                   "The Frequency  (default is 2.3 GHz).",
                   DoubleValue (2.3e9),
                   MakeDoubleAccessor (&ErcegPropagationLossModel::m_frequency),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("BSAntennaHeight",
                   " BS Antenna Height (default is 50m).",
                   DoubleValue (50.0),
                   MakeDoubleAccessor (&ErcegPropagationLossModel::m_BSAntennaHeight),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("SSAntennaHeight",
                   " SS Antenna Height (default is 3m).",
                   DoubleValue (3),
                   MakeDoubleAccessor (&ErcegPropagationLossModel::m_SSAntennaHeight),
                   MakeDoubleChecker<double> ())

    .AddAttribute ("MinDistance",
                   "The distance under which the propagation model refuses to give results (m) ",
                   DoubleValue (0.5),
                   MakeDoubleAccessor (&ErcegPropagationLossModel::SetMinDistance, &ErcegPropagationLossModel::GetMinDistance),
                   MakeDoubleChecker<double> ());
  return tid;
}

ErcegPropagationLossModel::ErcegPropagationLossModel ()
{
  C = 0;
  //changed m_shadowing from 10 to 0, Fariba 17Dec2013
  m_shadowing = 10;
}

void
ErcegPropagationLossModel::SetLambda (double frequency, double speed)
{
  m_lambda = speed / frequency;
  m_frequency = frequency;
}

double
ErcegPropagationLossModel::GetShadowing (void)
{
  return m_shadowing;
}
void
ErcegPropagationLossModel::SetShadowing (double shadowing)
{
  m_shadowing = shadowing;
}

void
ErcegPropagationLossModel::SetLambda (double lambda)
{
  m_lambda = lambda;
  m_frequency = 300000000 / lambda;
}

double
ErcegPropagationLossModel::GetLambda (void) const
{
  return m_lambda;
}

void
ErcegPropagationLossModel::SetMinDistance (double minDistance)
{
  m_minDistance = minDistance;
}
double
ErcegPropagationLossModel::GetMinDistance (void) const
{
  return m_minDistance;
}

void
ErcegPropagationLossModel::SetBSAntennaHeight (double height)
{
  m_BSAntennaHeight = height;
}

double
ErcegPropagationLossModel::GetBSAntennaHeight (void) const
{
  return m_BSAntennaHeight;
}

void
ErcegPropagationLossModel::SetSSAntennaHeight (double height)
{
  m_SSAntennaHeight = height;
}

double
ErcegPropagationLossModel::GetSSAntennaHeight (void) const
{
  return m_SSAntennaHeight;
}

void
ErcegPropagationLossModel::SetEnvironment (Environment env)
{
  m_environment = env;
}
ErcegPropagationLossModel::Environment
ErcegPropagationLossModel::GetEnvironment (void) const
{
  return m_environment;
}

double
ErcegPropagationLossModel::GetLoss (Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
	Ptr<MobilityModel> mmtmp;
	if (a->m_nodeName.compare("The_BS")==0)
	{
		NS_LOG_DEBUG("Swapping " << a->m_nodeName << "<-->" << b->m_nodeName);
		mmtmp = a;
		a = b;
		b = mmtmp;
	}

		double distance = a->GetDistanceFrom (b);
		NS_ASSERT(a->m_txFreq == b->m_txFreq);
	//  double log_f = std::log (a->m_txFreq) / 2.302;
		double wavelength = 300/a->m_txFreq; //3 * std::pow(10,8) / a->m_txFreq*std::pow(10,6);
		double d0 = 100;
	 // double alpha = 5.2; //dB
		double A = 20 *std::log (4 * 3.14159 * d0/wavelength) / 2.302; //free space pathloss
		double ae = 3.6;//4
		double be = 0.005;//0.0065
		double c = 20;//17.1
		double hb = b->m_antennaHeight;
	 // double x = 0.5, y = 0.1, z = 0.5;
	 // double sigmay = 0.75, musigma = 9.6, sigmasigma = 3;
		double Gamma = (ae - be * hb + c / hb) ;//+ x * sigmay; //path loss component
	 // double Xf = 6 * std::log(a->m_txFreq/2000) / 2.302;
	 // double hr = b->m_antennaHeight;
	 // double Xh = -20*log(hr/2) / 2.302;//10.8
	 // double sigma = musigma + z * sigmasigma;
	 // double S = y * sigma;//0.65 * std::pow((log_f),2) - 1.3 * (log_f) + alpha;
	 // double S= 8.2; //dB
	 // double k = 4;
		double deltaf = 6 * std::log (a->m_txFreq/2000) / 2.302 ;
		double deltah = -20 * std::log (a->m_antennaHeight/2) / 2.303;//10.8
		double loss_in_db = A + 10 * Gamma* (std::log (distance*1000/d0)) / 2.302 + deltaf + deltah;


// NS_LOG_UNCOND(b->m_nodeName << " a->m_txFreq=" <<  a->m_txFreq <<
													 //   ",wavelength=" << wavelength << ",A=" << A << ",Gamma=" << Gamma <<
														// ",deltaf=" << deltaf << ",deltah=" << deltah <<
														//   ", dist =" << distance << ", Path Loss = "<< loss_in_db);

		NS_LOG_DEBUG( "Heyyyyyyyyyyyyy" << b->m_nodeName << ", dist = ," << distance << ", Path Loss = ,"<< loss_in_db);
		return (0 - loss_in_db);

  }

double
ErcegPropagationLossModel::DoCalcRxPower (double txPowerDbm, Ptr<MobilityModel> a, Ptr<MobilityModel> b) const
{
	double Loss = GetLoss (a, b);
	NS_LOG_DEBUG("Heyyyyyyyyyyyyy! DoCalcRxPower: txPowerDbm=" << txPowerDbm
			<< ", a->m_nodeName=" << a->m_nodeName  << ",b->m_nodeName=" << b->m_nodeName
			<<  ", Loss=" << Loss << ", RxPow=" << txPowerDbm + Loss + (12+16));
  return txPowerDbm + Loss + (12+16);  //12,16 are TX/RX gains.
}

int64_t
ErcegPropagationLossModel::DoAssignStreams (int64_t stream)
{
  return 0;
}

}
